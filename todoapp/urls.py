from django.contrib import admin
from django.urls import path
from todoapp import views

urlpatterns = [
    path('todo', views.todo, name='todo'),
    path('show', views.show, name='show'),
    path('edit/<int:id>', views.edit, name='edit'),
    path('update/<int:id>', views.update, name='update'),
    path('delete/<int:id>', views.destroy, name='destroy'),
]
