from django.shortcuts import render, redirect
from todoapp.forms import TodoForm
from todoapp.models import Todoapp


def todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('show')
            except:
                pass
    else:
        form = TodoForm()
    return render(request, 'index.html', {'form': form})


def show(request):
    tasks = Todoapp.objects.all()
    return render(request, "show.html", {'tasks': tasks})


def edit(request, id):
    task = Todoapp.objects.get(id=id)
    return render(request, 'edit.html', {'task': task})


def update(request, id):
    task = Todoapp.objects.get(id=id)
    form = TodoForm(request.POST, instance=task)
    if form.is_valid():
        form.save()
        return redirect("show")
    return render(request, 'edit.html', {'task': task})


def destroy(request, id):
    task = Todoapp.objects.get(id=id)
    task.delete()
    return redirect("show")
