from django.db import models


class Todoapp(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=15)
    is_complete = models.BooleanField(default=False)

    class Meta:
        db_table = "tasks"

    def done(self):
        if self.is_complete is True:
            return 'Task is complete'
        return 'Task is incomplete'
