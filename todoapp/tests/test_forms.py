from django.test import SimpleTestCase
from todoapp.forms import *


class TestForms(SimpleTestCase):

    def test_todo_form(self):
        form = TodoForm(data={
            'title': 'task1',
            'description': 'gcjehci'
        })
        self.assertTrue(form.is_valid())

    def test_todo_no_data(self):
        form = TodoForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 2)
