from django.test import SimpleTestCase
from django.urls import reverse, resolve
from todoapp.views import *


class TestUrls(SimpleTestCase):

    def test_show_url_is_resolved(self):
        url = reverse('show')
        assert resolve(url).func, show

    def test_todo_url_is_resolved(self):
        url = reverse('todo')
        assert resolve(url).func, todo

    def test_edit_url_is_resolved(self):
        url = reverse('edit', args=['1'])
        assert resolve(url).func, edit

    def test_update_url_is_resolved(self):
        url = reverse('update', args=['1'])
        assert resolve(url).func, update

    def test_destroy_url_is_resolved(self):
        url = reverse('destroy', args=['1'])
        assert resolve(url).func, destroy
