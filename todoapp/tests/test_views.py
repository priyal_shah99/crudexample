import json
from django.test import TestCase, Client
from django.urls import reverse
from ..models import *


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.show_url = reverse('show')
        self.todo_url = reverse('todo')
        self.edit_url = reverse('edit', args=[4])
        self.update_url = reverse('update', args=[7])
        self.destroy_url = reverse('destroy', args=[2])

        Todoapp.objects.create(title='task1', description='this is some description here',
                               is_complete='False')

    def test_todo_show(self):
        response = self.client.get(self.show_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'show.html')

    def test_todo_todo(self):
        response = self.client.post(self.todo_url, {
            'title': 'task2',
            'description': 'this is description',

        })

        # t = Todoapp.objects.all()
        # print(t)
        # self.assertEqual(t.title,'task2')
        # for t in t2:
        #     print(t.id,"==========" ,t.title)
        self.assertEqual(response.status_code, 200)

    def test_todo_edit(self):
        # t = Todoapp.objects.all()
        # print(t)
        response = self.client.get(self.edit_url, {
            'id': 4
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'edit.html')

    def test_todo_update(self):
        # t = Todoapp.objects.all()
        # print(t)
        response = self.client.post(self.update_url, {
            'id': 7,
            'title': 'tasks',
            'description': 'jedjekdm',
            'is_complete': True
        })
        t = Todoapp.objects.get(id=7)
        print(t)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(t.title, 'tasks')

    def test_todo_destroy(self):
        Todoapp.objects.create(title='task3', description='hgdfr')

        response = self.client.delete(self.destroy_url, json.dumps({
            'id': 2
        }))
        self.assertEqual(response.status_code, 302)
        # self.assertTemplateUsed(response, 'todoapp/show.html')


import pytest
from mixer.backend.django import mixer
from ..views import todo
from django.test import RequestFactory
from django.contrib.auth.models import User


@pytest.mark.django_db
class TestViewsByPytest:
    def test_todo_authenticated(self):
        path = reverse('todo')
        request = RequestFactory().get(path)
        request.user = mixer.blend(User)

        response = todo(request)
        assert response.status_code == 200
