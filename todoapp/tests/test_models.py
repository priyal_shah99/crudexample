from django.test import TestCase
from mixer.backend.django import mixer
from todoapp.models import Todoapp


class TestModels(TestCase):

    def setUp(self):
        self.todo1 = Todoapp.objects.create(
            title='task1',
            description='bfjehfie',
            is_complete=True
        )

    def test_todo_task_status(self):
        self.assertEqual(self.todo1.done(), 'Task is complete')

    # def test_todo_title(self):
    #     t = mixer.blend(Todoapp,title='task3')
    #     assert t.title == 'task3'